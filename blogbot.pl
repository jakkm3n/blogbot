#!/usr/bin/perl -w
# blogbot.pl by jakk h bear

use Modern::Perl 2012;
use common::sense;
use autodie;

use Socket;
use Data::Dumper;

use AnyEvent;
use AnyEvent::IRC::Client;
use AnyEvent::IRC::Util qw/prefix_nick/;

use JSON;
use LWP::Simple;
use YAML::Tiny qw/Dump LoadFile/;

use utf8;
use Encode;
use HTML::Entities;
binmode STDOUT, ':utf8';

my $configfile = 'blog.conf';
my $dbfile = 'blog.db';

my $exit = AnyEvent->condvar;
my $bot = AnyEvent::IRC::Client->new;

my $conf = LoadFile($configfile) or die "Could not open $configfile";
my @reddits;
@reddits = values $conf->{reddit} if defined $conf->{reddit};
my @twitters;
@twitters = values $conf->{twitter} if defined $conf->{twitter};

makedb() unless (-e $dbfile);
my $db = LoadFile($dbfile);

my $owner = $conf->{owner};
my $nick = $conf->{nick};
my $server = $conf->{server};
my @channels = values $conf->{channels};
my $port = $conf->{port};
my $delay = $conf->{delay};
my $limit = $conf->{limit};

my $ip = inet_ntoa(inet_aton($server) or die 'Unknown host');

sub irccon {
	say "Connecting to $server:$port as $nick";
	$bot->connect($ip, $port, { nick => $nick, user => $nick, real => $nick });
	$bot->send_srv('JOIN', $_) for @channels;
}	

sub makedb {
	say "-!- Making database";

	my $newdb;
	$newdb->{reddit} = {};
	$newdb->{twitter} = {};
	$newdb->{ids} = {};

	open DB, '>', $dbfile;
	print DB Dump($newdb);
	close DB;
}

sub reddit {
	my $reddit = lc $_[0];
	my $reqnum = $_[1] if $_[1];
	my $num = 1;
	$num = $reqnum if $reqnum;
	$num = 1 if $num < 0;
	$num = $limit if $reqnum && $reqnum > $limit;

	say "Checking $reddit";
	my $url = 'http://www.reddit.com/r/' . $reddit . '.json?sort=new';
	my $data = get($url);
	return unless (defined $data);
	return if ($data =~ /^<!DOCTYPE/);
	my $feed = decode_json($data) or return;

	my @messages;
	$num = $num - 1;
	for (0..$num) {
		my $message = $feed->{data}{children}[$_]{data}{title};
		return unless (defined $message);
		my $id = $feed->{data}{children}[$_]{data}{id};

		return if $db->{ids}{$id};
		$db->{ids}{$id} = 1;

		say 'New message';
		$message = encode('utf8', $message);
		$message = decode_entities($message);
		push (@messages, $message);
	}

	for my $channel (@channels) {
		$bot->send_srv('PRIVMSG', $channel, $_) for @messages;
	}

	$db->{reddit}{$reddit} = [] unless (defined $db->{reddit}{$reddit});
	push ($db->{reddit}{$reddit}, $_) for @messages;
}	

sub twitter {
	my $twitter = lc $_[0];
	my $reqnum = $_[1] if $_[1];
	my $num = 1;
	$num = $reqnum if $reqnum;
	$num = 1 if $num < 0;
	$num = $limit if $reqnum && $reqnum > $limit;

	say "Checking $twitter";
	my $url = 'http://api.twitter.com/1/statuses/user_timeline.json?screen_name=' . $twitter . '&count=5';
	my $data = get($url);
	return unless (defined $data);
	my @feed = decode_json($data);

	my @messages;
	$num = $num - 1;
	for (0..$num) {  
		my $message = $feed[0][$_]->{text};
		return unless (defined $message);
		my $id = $feed[0][$_]->{id_str};

		return if $db->{ids}{$id};
		$db->{ids}{$id} = 1;

		say 'New message';
		$message = encode('utf8', $message);
		$message = decode_entities($message);
		push (@messages, $message);
	}

	for my $channel (@channels) {
		$bot->send_srv('PRIVMSG', $channel, $_) for @messages;
	}

	$db->{twitter}{$twitter} = [] unless (defined $db->{twitter}{$twitter});
	push ($db->{twitter}{$twitter}, $_) for @messages;
}

$bot->reg_cb(connect => sub { say "Connected to $server"; $bot->enable_ping(60); });
$bot->reg_cb(disconnect => sub { irccon(); });
$bot->send_srv('MODE', $nick, '-iwx');
$bot->reg_cb(join => sub { say "Joined $_[2]" if $_[1] eq $nick; });
$bot->reg_cb(debug_send => sub { my ($self, $msg) = @_; say "--> $msg"; });

$bot->ctcp_auto_reply('VERSION', ['VERSION', 'mIRC v7.11 Khaled Mardam-Bey']);

$bot->reg_cb (
	debug_recv => sub {
		my ($self, $msg) = @_;
		return unless ($msg->{command} eq 'PRIVMSG');
		return unless ($msg->{params}[1] =~ /^!/);

		my $user = prefix_nick($msg->{prefix});
		my @params = split(' ', $msg->{params}[1]);

		if ($params[0] eq '!reddit') {
			my $reddit = $params[1];
			my $num = 1;
			$num = $params[2] if $params[2];
			my $s = '';
			$s .= 's' if $num > 1;
			say "--- Request for $reddit by $user, $num line$s";

			reddit($reddit, $num);
		}

		if ($params[0] eq '!twitter') {
			my $twitter = $params[1];
			my $num = 1;
			$num = $params[2] if $params[2];
			my $s = '';
			$s .= 's' if $num > 1;
			say "--- Request for $twitter by $user, $num line$s";
			twitter($twitter, $num);
		}

		if ($params[0] eq '!update') {
			say "--- Request to update by $user";
			reddit($_) for @reddits;
			twitter($_) for @twitters;
		}

		if ($params[0] eq '!quit') {
			say "--- Request to quit by $user ---";
			return unless $user eq $owner;
			say "Disconnecting";
			$exit->broadcast;
		}
	}
);

irccon();

my $reddittimer = AnyEvent->timer(
	after => 10,
	interval => $delay,
	cb => sub {
		say "--- Updating reddit";
		reddit($_) for @reddits;
	}				
);

my $twittertimer = AnyEvent->timer(
	after => 10,
	interval => $delay,
	cb => sub {
		say "--- Updating twitter";
		twitter($_) for @twitters;
	}
);

my $dbtimer = AnyEvent->timer(
	after => 20,
	interval => $delay * 2,
	cb => sub {
		say '-!- Syncing database';

		my $sync = Dump($db);
		$sync = encode('utf8', $sync);
		$sync = decode_entities($sync);

		open DB, '>', $dbfile;
		print DB $sync;
		close DB;
	}
);

$exit->wait;
say 'Finished';
